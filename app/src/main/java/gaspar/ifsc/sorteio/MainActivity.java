package gaspar.ifsc.sorteio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    EditText edMinimo;
    EditText edMax;

    TextView tvResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edMinimo = (EditText) findViewById(R.id.edMin);
        edMax = (EditText) findViewById(R.id.edMax);
        tvResultado = (TextView) findViewById(R.id.tvResultado);
    }

    public void sorteiaNumero(View v){

        int valorMin = Integer.parseInt(this.edMinimo.getText().toString());
        int valorMax = Integer.parseInt(this.edMax.getText().toString());

        int x = new Random().nextInt(valorMax-valorMin)+valorMin;

        tvResultado.setText(Integer.toString(x));
    }
}
